#include <pthread.h>
#include <assert.h>

#include "util.h"
#include "mm-thread.h"

extern void *mm_thread(void *ptr);
extern void *mm_thread_l1(void *ptr);
extern void *mm_thread_l1_sse(void *ptr);

/* user input (configuration) struct */
struct mm_conf {
	void * (*thread_function)(void*);
	int thread_count;
	int Ni;
	int Nj;
	int Nk;
};

void mm_start_threads(mat_type_t *a, mat_type_t *b, mat_type_t *c, struct mm_conf conf);
void read_args(int argc, const char **argv, struct mm_conf * conf);

int main(int argc, const char **argv) {
	mat_type_t *a, *b, *c;

	if (argc != 6) {
		printf("usage: N M K THREAD_COUNT FUNCTION\n");
		printf("\nTHREAD_COUNT:[1,%d]\n", MAX_THREAD_COUNT);
		printf("\nFUNCTIONS:\n 0->standard\n 1->l1-optimized\n 2->l1-sse-optimized\n");
		return 1;
	}

	FILE *fa = fopen("A.mat.txt", "w");
	FILE *fb = fopen("B.mat.txt", "w");
	FILE *fc = fopen("C.mat.txt", "w");

	struct mm_conf conf;
	read_args(argc, argv, &conf);

	printf("%dx%d * %dx%d matrix multiplication\n", conf.Ni, conf.Nj, conf.Nj, conf.Nk);

	matrix_malloc(&a, &b, &c, conf.Ni, conf.Nj, conf.Nk, 1<<12);
	printf("random A & B matrix generation\n");
	random_matrix(a, conf.Ni, conf.Nj);
	random_matrix(b, conf.Nj, conf.Nk);
	printf("saving A & B matrix\n");
	save_matrix(fa, a, conf.Ni, conf.Nj);
	save_matrix(fb, b, conf.Nj, conf.Nk);

	printf("matrix multiply started\n");
	struct timeval start, end;
	gettimeofday(&start, NULL);
	mm_start_threads(a, b, c, conf);
	gettimeofday(&end, NULL);
	printf("time: %ld\n", elapsed_milliseconds(start, end));

	printf("saving C matrix\n");
	save_matrix(fc, c, conf.Ni, conf.Nk);

	fclose(fa);
	fclose(fb);
	fclose(fc);

	return 0;
}


void mm_start_threads(mat_type_t *a, mat_type_t *b, mat_type_t *c, struct mm_conf conf) {
	int i;
	struct Job job = {0, 0, 0, conf.Ni, conf.Nj, conf.Nk};

	struct Job * thread_jobs;
	pthread_t thread[MAX_THREAD_COUNT];
	struct ThreadData td[MAX_THREAD_COUNT];

	/* split Ni x Nj x Nk loop for threads (thread count = "conf.thread_count")
	   ## example ##
	   2000x4000 * 4000x3000 matrix multiplication
	   Ni = 2000, Nj=4000, Nk=3000
	   thread_count: 2
	   >> output thread jobs:
	   thread 1: 1000x4000 * 4000x3000 multiplication
	   thraed 2: 1000x4000 * 4000x3000 multiplication
	 */
	split_job_i(&thread_jobs, job, conf.thread_count, conf.Ni, conf.Nj, conf.Nk);

	/* print threads jobs */
	for (i = 0; i < conf.thread_count ; i++) {
		printf("thread_%d: %dx%d * %dx%d matrix multiplication\n", i, thread_jobs[i].Ni,
			   thread_jobs[i].Nj, thread_jobs[i].Nj, thread_jobs[i].Nk);
	}

	/* start all threads */
	for (i = conf.thread_count-1; i >= 0 ; i--) {
		/*
		  td: thread job data (matrix address, start index, size ...)
		  conf.thread_function: chosen matrix multiplication function
		*/
		td[i] = (struct ThreadData) { a, b, c, thread_jobs, i, conf.Ni, conf.Nj, conf.Nk };
		pthread_create(&thread[i], NULL, conf.thread_function, (void*)&td[i]);
	}

	/* wait all threads exit */
	for (i = 0; i < conf.thread_count ; i++)
		pthread_join(thread[i], NULL);
}

void read_args(int argc, const char **argv, struct mm_conf * conf) {
	conf->Ni = atoi(argv[1]);
	conf->Nj = atoi(argv[2]);
	conf->Nk = atoi(argv[3]);
	conf->thread_count = atoi(argv[4]);

	switch (atoi(argv[5])) {
	case 0:
		conf->thread_function = mm_thread;
		break;
	case 1:
		conf->thread_function = mm_thread_l1;
		assert(conf->Ni % L1_count == 0);
		assert(conf->Nj % L1_count == 0);
		assert(conf->Nk % L1_count == 0);
		break;
	case 2:
		conf->thread_function = mm_thread_l1_sse;
		assert(conf->Ni % L1_count == 0);
		assert(conf->Nj % L1_count == 0);
		assert(conf->Nk % L1_count == 0);
		break;
	default:
		printf("wrong FUNCTION\n");
		exit(1);
	}
}
