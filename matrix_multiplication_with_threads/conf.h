#ifndef _CONF_H_
#define _CONF_H_

typedef float mat_type_t;

/* L1, L2 ve L3 icin N. Bu degiskenler sadece mm-loop_blocking_lX.c ile kullanilir
   L3_count=400 -> 400*400*4*3 byte yer kaplar. */
#define L3_count 200
#define L2_count 200
#define L1_count 40

#define MAX_THREAD_COUNT 64

#ifndef MATRIX_PADDING
# define MATRIX_PADDING 0
//# warning "matrix padding not defined. using 0"
#endif

/* L3 ve L2 boyutlarinin birbirinin kati olmasini kontrol et. */
#if (L3_count % L2_count) != 0
# error "(L3_count % L2_count) != 0"
#endif
#if (L2_count % L1_count) != 0
# error "(L1_count % L2_count) != 0"
#endif
/* */

#endif /* _CONF_H_ */
