#include "util.h"
#include "mm-thread.h"

#define A(x,y,N) a[__2d_index(x,y,N)]
#define B(x,y,N) b[__2d_index(x,y,N)]
#define C(x,y,N) c[__2d_index(x,y,N)]

void *mm_thread(void *ptr) {
	int i,j,k;
	struct ThreadData *td = (struct ThreadData*)ptr;

	/* int _Ni = td->_Ni; */
	int _Nj = td->_Nj;
	int _Nk = td->_Nk;
	int no = td->jno;
	mat_type_t *a = td->a + td->jobs[no].a_start;

	mat_type_t *b = td->b + td->jobs[no].b_start;
	mat_type_t *c = td->c + td->jobs[no].c_start;
	/* printf("[%d] %d - %d - %d\n", td->jno, Ni, Nj, Nk); */

	const int job_Ni = td->jobs[no].Ni;
	const int job_Nj = td->jobs[no].Nj;
	const int job_Nk = td->jobs[no].Nk;

	for (i = 0 ; i < job_Ni ; i++)
		for (k = 0 ; k < job_Nk ; k++)
			C(i, k, _Nk) = 0;

	for (i = 0 ; i < job_Ni ; i++)
		for (j = 0 ; j < job_Nj ; j++)
			for (k = 0 ; k < job_Nk ; k++)
				C(i,k,_Nk) += A(i,j,_Nj) * B(j,k,_Nk);

	return NULL;
}

#undef A
#undef B
#undef C
