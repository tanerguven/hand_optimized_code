#include <stdio.h>
#include <assert.h>

#include "util.h"
#define XN (4*1024*1024)

/* 16 MB */
int X[XN];

/* 16 MB boutundaki X isimli vektor uzerinde rast gele degisiklik yapar ve
   tum vektoru tarayan bir islem yaparak matrisleri (A,B,C) cachelerden siler */
void flush_cache() {
	printf("\nflush cache...");
	int i;
	/* 0. ve 10 tane rastgele adresi degistirir */
	X[0] = rand();
	for (i = 0 ; i < 10 ; i++)
		X[rand() % XN] = rand();
	for (i = 1 ; i < XN-1 ; i++) {
		X[i] = X[i-1] + X[i + 1] ;
	}
	printf("OK\n");
}

void random_matrix(mat_type_t *m, int Ni, int Nj) {
	int i,j;
	for (i = 0 ; i < Ni ; i++) {
		for (j = 0 ; j < Nj ; j++) {
			m[__2d_index(i,j,Nj)] = random_mat_type();
		}
	}
}

void save_matrix(FILE *f, mat_type_t *m, int Ni, int Nj) {
	int i, j;
#define M(x,y) m[__2d_index(x,y,Nj)]
	for (i = 0 ; i < Ni ; i++) {
		for (j = 0 ; j < Nj ; j++) {
			fprintf(f, "%f ", M(i,j));
		}
		fprintf(f,"\n");
	}
#undef M
}

void read_matrix_bin_i(mat_type_t *m, int _Ni, int _Nj, FILE *f, int start) {
	fseek(f, start*sizeof(mat_type_t), SEEK_SET);
	fread(m, sizeof(mat_type_t), _Ni*_Nj, f);
}

void save_matrix_bin(const mat_type_t *m, int _Ni, int _Nj, FILE *f) {
	fwrite(m, sizeof(mat_type_t), _Ni*_Nj, f);
}

void save_matrix_bin_append(const mat_type_t *m, int _Ni, int _Nj, FILE *f) {
	fwrite(m, sizeof(mat_type_t), _Ni*_Nj, f);
}


void split_job_i(struct Job **jobs, struct Job job, int jc, int _Ni, int _Nj, int _Nk) {
	int i;
	mat_type_t _a[1]; // adres hesaplamasi icin sahte matris
	mat_type_t _b[1];
	mat_type_t _c[1];
	mat_type_t *a = &_a[job.a_start];
	mat_type_t *b = &_b[job.b_start];
	mat_type_t *c = &_c[job.c_start];
#define A(x,y) a[__2d_index(x,y,_Nj)]
#define B(x,y) b[__2d_index(x,y,_Nk)]
#define C(x,y) c[__2d_index(x,y,_Nk)]
	*jobs = (struct Job*) malloc(sizeof(struct Job)*(jc));
	int verilen=0;
	for (i = 0; i < jc ; i++) {
		int kalan = job.Ni-verilen;
		int Ni = (kalan / (jc-i)) - (kalan / (jc-i)) % L1_count;
		((*jobs)[i]) = (struct Job) {&A(verilen,0)-_a, &B(0,0)-_b, &C(verilen,0)-_c, Ni, job.Nj, job.Nk};
		verilen += Ni;
	}
	/* printf("%d - %d\n", verilen, job.Ni); */
	assert(verilen == job.Ni);
#undef A
#undef B
#undef C
}

void split_job_j(struct Job **jobs, struct Job job, int jc, int _Ni, int _Nj, int _Nk) {
	int j;
	mat_type_t _a[1];
	mat_type_t _b[1];
	mat_type_t _c[1];
	mat_type_t *a = &_a[job.a_start];
	mat_type_t *b = &_b[job.b_start];
	mat_type_t *c = &_c[job.c_start];
#define A(x,y) a[__2d_index(x,y,_Nj)]
#define B(x,y) b[__2d_index(x,y,_Nk)]
#define C(x,y) c[__2d_index(x,y,_Nk)]
	*jobs = (struct Job*) malloc(sizeof(struct Job)*(jc));
	int verilen=0;
	for (j = 0; j < jc ; j++) {
		int kalan = job.Nj-verilen;
		int Nj = (kalan / (jc-j)) - (kalan / (jc-j)) % L1_count;
		((*jobs)[j]) = (struct Job) {&A(0,0)-_a, &B(0,verilen)-_b, &C(0,verilen)-_c, job.Ni, Nj, job.Nk};
		verilen += Nj;
	}
	assert(verilen == job.Nj);
#undef A
#undef B
#undef C
}

void transpose_matrix(mat_type_t *m, int N, int M) {
	assert(N == M);
#define M(x,y) m[__2d_index(x,y,N)]
	int i, j;
	for (i = 0 ; i < N ; i++) {
		for (j = i ; j < N ; j++) {
			mat_type_t tmp = M(i,j);
			M(i,j) = M(j,i);
			M(j,i) = tmp;
		}
	}
#undef M
}

void matrix_malloc(mat_type_t **a, mat_type_t **b, mat_type_t **c, int _Ni, int _Nj, int _Nk, int padding) {
	uint64_t _a = (uint64_t)malloc(sizeof(mat_type_t)*_Ni*_Nj+padding);
	uint64_t _b = (uint64_t)malloc(sizeof(mat_type_t)*_Nj*_Nk+padding);
	uint64_t _c = (uint64_t)malloc(sizeof(mat_type_t)*_Ni*_Nk+padding);

	/* printf("a: 0x%lx\n", _a); */
	/* printf("b: 0x%lx\n", _b); */
	/* printf("c: 0x%lx\n", _c); */

	if (padding) {
		/* printf("padding: %d\n", padding); */
		*a = (mat_type_t*)(_a - (_a % padding) + padding);
		*b = (mat_type_t*)(_b - (_b % padding) + padding);
		*c = (mat_type_t*)(_c - (_c % padding) + padding);
		/* printf("a: %p\n", a); */
		/* printf("b: %p\n", b); */
		/* printf("c: %p\n", c); */

	} else {
		*a = (mat_type_t*)_a;
		*b = (mat_type_t*)_b;
		*c = (mat_type_t*)_c;
	}
}
