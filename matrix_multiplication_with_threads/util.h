#ifndef _UTIL_H_
#define _UTIL_H_

#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "conf.h"

#define random_mat_type() ((mat_type_t)rand() / RAND_MAX)
#define __2d_index(i, j, n) (i*n + j)

static inline unsigned long elapsed_milliseconds(struct timeval s, struct timeval e) {
	return (e.tv_sec - s.tv_sec)*1000 + (e.tv_usec - s.tv_usec)/1000;
}

void matrix_malloc(mat_type_t **a, mat_type_t **b, mat_type_t **c, int Ni, int Nj, int Nk, int padding);
void flush_cache();
void random_matrix(mat_type_t *m, int N, int M);
void save_matrix(FILE *f, mat_type_t *m, int N, int M);
void read_matrix_bin_i(mat_type_t *m, int Ni, int Nj, FILE *f, int Si);
void save_matrix_bin(const mat_type_t *m, int N, int M, FILE *f);
void save_matrix_bin_append(const mat_type_t *m, int Ni, int Nj, FILE *f);

void transpose_matrix(mat_type_t *m, int N, int M);

struct Job {
	int a_start;
	int b_start;
	int c_start;
	int Ni;
	int Nj;
	int Nk;
};

void split_job_i(struct Job **jobs, struct Job job, int jc, int Ni, int Nj, int Nk);
void split_job_j(struct Job **jobs, struct Job job, int jc, int Ni, int Nj, int Nk);

#endif /* _UTIL_H_ */
