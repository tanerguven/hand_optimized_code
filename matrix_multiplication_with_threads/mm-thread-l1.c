#include <pthread.h>
#include <assert.h>

#include "util.h"
#include "mm-thread.h"

#define A(x,y,N) a[__2d_index(x,y,N)]
#define B(x,y,N) b[__2d_index(x,y,N)]
#define C(x,y,N) c[__2d_index(x,y,N)]

static inline void mm_L1(mat_type_t *c, mat_type_t * const a, mat_type_t * const b, const int _Ni, const int _Nj, const int _Nk);

void *mm_thread_l1(void *ptr) {
	int i,j,k;
	struct ThreadData *td = (struct ThreadData*)ptr;

	int _Ni = td->_Ni;
	int _Nj = td->_Nj;
	int _Nk = td->_Nk;
	int no = td->jno;
	mat_type_t *a = td->a + td->jobs[no].a_start;

	mat_type_t *b = td->b + td->jobs[no].b_start;
	mat_type_t *c = td->c + td->jobs[no].c_start;
	/* printf("[%d] %d - %d - %d\n", td->jno, Ni, Nj, Nk); */

	const int job_Ni = td->jobs[no].Ni;
	const int job_Nj = td->jobs[no].Nj;
	const int job_Nk = td->jobs[no].Nk;

	assert(job_Ni % L1_count == 0);
	assert(job_Nj % L1_count == 0);
	assert(job_Nk % L1_count == 0);

	for (i = 0 ; i < job_Ni ; i++)
		for (k = 0 ; k < job_Nk ; k++)
			C(i, k, _Nk) = 0;

	for (i = 0 ; i < job_Ni ; i+=L1_count)
		for (j = 0 ; j < job_Nj ; j+=L1_count)
			for (k = 0 ; k < job_Nk ; k+=L1_count)
				mm_L1( &C(i,k,_Nk), &A(i,j,_Nj), &B(j,k,_Nk), _Ni, _Nj, _Nk);

	return NULL;
}

static inline void mm_L1(mat_type_t *c, mat_type_t * const a, mat_type_t * const b, const int _Ni, const int _Nj, const int _Nk) {
	int i,j,k;

	for (i = 0 ; i < L1_count ; i++)
		for (j = 0 ; j < L1_count ; j++)
			for (k = 0 ; k < L1_count ; k++)
				C(i,k,_Nk) += A(i,j,_Nj) * B(j,k,_Nk);
}

#undef A
#undef B
#undef C
