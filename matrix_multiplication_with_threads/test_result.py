#!/usr/bin/python
import time
import numpy as np
import subprocess

#subprocess.call(['./convert_files.x'])

A = np.loadtxt("A.mat.txt", dtype=np.float32)
B = np.loadtxt("B.mat.txt", dtype=np.float32)
C = np.loadtxt("C.mat.txt", dtype=np.float32)

print A.shape, B.shape, C.shape

t1 = time.time()
C_correct = np.dot(A, B)
t2 = time.time()

print "numpy multiplication time:", (t2-t1)*1000

print np.allclose(C, C_correct)
