hand optimized matrix multiplication with threads
=================================================

optimization modes:  
0 -> standart 3 for loop  
1 -> l1 size loop blocking  
2 -> l1 size loop blocking & sse  

## Tests ##

### test system ###
CPU: Intel(R) Core(TM) i3 CPU M 380  @ 2.53GHz  
compiler: gcc-4.7  
compiler flags: -Ofast -march=native  
code version: commit 99ff7ccdf50da19881960e4174d99053d62ac282  

### 4000x4000 * 4000x4000 Matrix Multiplication Tests ###

1 thread optimization=2

	./mm.x 4000 4000 4000 1 2
	4000x4000 * 4000x4000 matrix multiplication
	random A & B matrix generation
	saving A & B matrix
	matrix multiply started
	thread_0: 4000x4000 * 4000x4000 matrix multiplication
	time: 15183
	saving C matrix

2 thread optimization=2

	./mm.x 4000 4000 4000 2 2
	4000x4000 * 4000x4000 matrix multiplication
	random A & B matrix generation
	saving A & B matrix
	matrix multiply started
	thread_0: 2000x4000 * 4000x4000 matrix multiplication
	thread_1: 2000x4000 * 4000x4000 matrix multiplication
	time: 8068
	saving C matrix

1 thread python numpy

	python test_result.py
	(4000, 4000) (4000, 4000) (4000, 4000)
	numpy multiplication time: 16471.2231159
	True

### 3920x3960 * 3960x4280 Matrix Multiplication Tests ###

1 thread optimization=2

	./mm.x 3920 3960 4280 1 2
	3920x3960 * 3960x4280 matrix multiplication
	random A & B matrix generation
	saving A & B matrix
	matrix multiply started
	thread_0: 3920x3960 * 3960x4280 matrix multiplication
	time: 15822
	saving C matrix

2 thread optimization=2

	./mm.x 3920 3960 4280 2 2
	3920x3960 * 3960x4280 matrix multiplication
	random A & B matrix generation
	saving A & B matrix
	matrix multiply started
	thread_0: 1960x3960 * 3960x4280 matrix multiplication
	thread_1: 1960x3960 * 3960x4280 matrix multiplication
	time: 8315
	saving C matrix

1 thread python numpy

	python test_result.py
	(3920, 3960) (3960, 4280) (3920, 4280)
	numpy multiplication time: 16991.435051
	True

1 thread standart (optimization=0)

	./mm.x 3920 3960 4280 1 0
	3920x3960 * 3960x4280 matrix multiplication
	random A & B matrix generation
	saving A & B matrix
	matrix multiply started
	thread_0: 3920x3960 * 3960x4280 matrix multiplication
	time: 48274
	saving C matrix
