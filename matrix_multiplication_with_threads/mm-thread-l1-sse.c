#include "util.h"
#include "mm-thread.h"

#include <assert.h>
#include <pthread.h>
#include <stdint.h>

#ifdef __INTEL_COMPILER
# include <tmmintrin.h>
#else
# include <immintrin.h>
#endif

static inline void mm_L1(mat_type_t *c, mat_type_t * const a, mat_type_t * const b, const int _Ni, const int _Nj, const int _Nk);
static inline void mm_sse_8x4x4(mat_type_t *a, mat_type_t *b, mat_type_t *c, const int _Ni, const int _Nj, const int _Nk);

#define A(x,y,N) a[__2d_index(x,y,N)]
#define B(x,y,N) b[__2d_index(x,y,N)]
#define C(x,y,N) c[__2d_index(x,y,N)]


void *mm_thread_l1_sse(void *ptr) {
	int i,j,k;
	struct ThreadData *td = (struct ThreadData*)ptr;

	int _Ni = td->_Ni;
	int _Nj = td->_Nj;
	int _Nk = td->_Nk;

	int no = td->jno;
	mat_type_t *a = td->a + td->jobs[no].a_start;

	mat_type_t *b = td->b + td->jobs[no].b_start;
	mat_type_t *c = td->c + td->jobs[no].c_start;
	const int job_Ni = td->jobs[no].Ni;
	const int job_Nj = td->jobs[no].Nj;
	const int job_Nk = td->jobs[no].Nk;
	/* printf("[%d] %d - %d - %d\n", td->jno, Ni, Nj, Nk); */

	assert(job_Ni % L1_count == 0);
	assert(job_Nj % L1_count == 0);
	assert(job_Nk % L1_count == 0);

	for (i = 0 ; i < job_Ni ; i++)
		for (k = 0 ; k < job_Nk ; k++)
			C(i, k, _Nk) = 0;

	for (i = 0 ; i < job_Ni ; i += L1_count)
		for (j = 0 ; j < job_Nj ; j += L1_count)
			for (k = 0 ; k < job_Nk ; k += L1_count)
				mm_L1( &C(i,k,_Nk), &A(i,j,_Nj), &B(j,k,_Nk), _Ni, _Nj, _Nk);
	return NULL;
}

static inline void mm_L1(mat_type_t *c, mat_type_t * const a, mat_type_t * const b, const int _Ni, const int _Nj, const int _Nk) {
	int i,j,k;

	for (i = 0 ; i < L1_count ; i+=8)
		for (j = 0 ; j < L1_count ; j+=4)
			for (k = 0 ; k < L1_count ; k+=4)
				mm_sse_8x4x4(&A(i,j,_Nj), &B(j,k,_Nk), &C(i,k,_Nk), _Ni, _Nj, _Nk);
}

static inline void mm_sse_8x4x4(mat_type_t *a, mat_type_t *b, mat_type_t *c, const int _Ni, const int _Nj, const int _Nk) {

	/* su sayfadaki kod duzenlenerek eklendi
	   http://stackoverflow.com/questions/18499971/efficient-4x4-matrix-multiplication-c-vs-assembly */
	int i;

    __m128 b0 = _mm_load_ps(&B(0,0,_Nk));
    __m128 b1 = _mm_load_ps(&B(1,0,_Nk));
    __m128 b2 = _mm_load_ps(&B(2,0,_Nk));
    __m128 b3 = _mm_load_ps(&B(3,0,_Nk));

	mat_type_t *aa = &A(0,0,_Nj);
	mat_type_t *cc = &C(0,0,_Nk);

	for (i = 0; i < 8 ; i++) {
		__m128 a0 = _mm_set1_ps(*aa);
		__m128 a1 = _mm_set1_ps(*(aa+1));
		__m128 a2 = _mm_set1_ps(*(aa+2));
		__m128 a3 = _mm_set1_ps(*(aa+3));

		__m128 r = _mm_add_ps(
			_mm_load_ps(cc),
			_mm_add_ps(
				_mm_add_ps(
					_mm_mul_ps(a0, b0),
					_mm_mul_ps(a1, b1)),
				_mm_add_ps(
					_mm_mul_ps(a2, b2),
					_mm_mul_ps(a3, b3))));

		_mm_store_ps(cc, r);

		aa += _Nj;
		cc += _Nk;
	}
}

#if 0

static inline void mm_sse_8x4x4_const_size(mat_type_t *a, mat_type_t *b, mat_type_t *c) {
	// boyut sabit oldugunda daha iyi optimizasyon
	#define N 2000
	/* su sayfadaki kod duzenlenerek eklendi
	   http://stackoverflow.com/questions/18499971/efficient-4x4-matrix-multiplication-c-vs-assembly */
	int i;

	// FIXME: bir hata var

    __m128 b0 = _mm_load_ps(&B(0,0,N));
    __m128 b1 = _mm_load_ps(&B(1,0,N));
    __m128 b2 = _mm_load_ps(&B(2,0,N));
    __m128 b3 = _mm_load_ps(&B(3,0,N));

	for (i = 0; i < 8 ; i++) {
		__m128 a0 = _mm_set1_ps(*&A(i,0,N));
		__m128 a1 = _mm_set1_ps(*&A(i+1,0,N));
		__m128 a2 = _mm_set1_ps(*&A(i+2,0,N));
		__m128 a3 = _mm_set1_ps(*&A(i+3,0,N));

		__m128 r = _mm_add_ps(
			_mm_load_ps(&C(i,0,N)),
			_mm_add_ps(
				_mm_add_ps(
					_mm_mul_ps(a0, b0),
					_mm_mul_ps(a1, b1)),
				_mm_add_ps(
					_mm_mul_ps(a2, b2),
					_mm_mul_ps(a3, b3))));

		_mm_store_ps(&C(i,0,N), r);
	}
}

#endif

#undef A
#undef B
#undef C


