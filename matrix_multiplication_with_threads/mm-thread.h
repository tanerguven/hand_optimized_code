#ifndef _MM_THREAD_H_
#define _MM_THREAD_H_

struct ThreadData {
	mat_type_t *a;
	mat_type_t *b;
	mat_type_t *c;
	const struct Job *jobs;
	int jno;
	int _Ni, _Nj, _Nk;
};

#endif /* _MM_THREAD_H_ */
